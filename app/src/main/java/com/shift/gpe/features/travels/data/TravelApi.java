package com.shift.gpe.features.travels.data;

import com.shift.gpe.features.travels.domain.model.Success;
import com.shift.gpe.features.travels.domain.model.Travel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface TravelApi {

    @GET("travel")
    Call<List<Travel>> getTravelList();

    @GET("travel/{id}")
    Call<Travel> getTravel(@Path("id") String id);

    @POST("travel/{id}")
    Call<Success> setTravel(@Path("id") String id, @Body Travel travel);

    @POST("travel")
    Call<Travel> createTravel(@Body Travel travel);

    @DELETE("travel/{id}")
    Call<Success> deleteTravel(@Path("id") String id);

}
