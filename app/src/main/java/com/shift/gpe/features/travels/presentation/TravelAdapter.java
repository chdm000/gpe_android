package com.shift.gpe.features.travels.presentation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shift.gpe.R;
import com.shift.gpe.features.travels.domain.model.Travel;

import java.util.ArrayList;
import java.util.List;

public class TravelAdapter extends RecyclerView.Adapter<TravelAdapter.TravelHolder> {
    private final List<Travel> travels = new ArrayList<>();
    private final LayoutInflater inflater;
    private final SelectTravelListener selectTravelListener;

    TravelAdapter(Context context, SelectTravelListener selectTravelListener) {
        inflater = LayoutInflater.from(context);
        this.selectTravelListener = selectTravelListener;
    }

    @NonNull
    @Override
    public TravelHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View itemView = inflater.inflate(R.layout.travel_layout, parent, false);
        return new TravelHolder(itemView, selectTravelListener);
    }

    @Override
    public void onBindViewHolder(@NonNull TravelHolder holder, int position) {
        holder.bind(travels.get(position));
    }

    @Override
    public int getItemCount() {
        return travels.size();
    }

    public void setTravels(List<Travel> bookList) {
        travels.clear();
        travels.addAll(bookList);
        notifyDataSetChanged();
    }

    class TravelHolder extends RecyclerView.ViewHolder {

        //private final TextView travelName;
        private final SelectTravelListener selectTravelListener;

        TravelHolder(View view, SelectTravelListener selectTravelListener) {
            super(view);
            this.selectTravelListener = selectTravelListener;
            //bookNameView = view.findViewById(R.id.book_item_name);
            //bookAuthorView = view.findViewById(R.id.book_item_author);
        }

        void bind(final Travel travel) {
            //bookNameView.setText(book.getName());
            //bookAuthorView.setText(book.getAuthor());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectTravelListener.onTravelSelect(travel);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    selectTravelListener.onTravelLongClick(travel);
                    return true;
                }
            });

        }

    }

    interface SelectTravelListener {

        void onTravelSelect(Travel travel);

        void onTravelLongClick(Travel travel);

    }
}
