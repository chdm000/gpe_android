package com.shift.gpe.features.travels.domain;

import com.shift.gpe.features.travels.data.TravelsRepository;
import com.shift.gpe.features.travels.domain.model.Success;
import com.shift.gpe.features.travels.domain.model.Travel;
import com.shift.gpe.network.Carry;

import java.util.List;

public final class TravelsInteractorImpl implements TravelsInteractor {

    private final TravelsRepository repository;

    public TravelsInteractorImpl(TravelsRepository repository) {
        this.repository = repository;
    }

    @Override
    public void loadTravels(Carry<List<Travel>> carry) {
        repository.loadTravels(carry);
    }

    @Override
    public void loadTravel(String id, Carry<Travel> carry) {
        repository.loadTravel(id, carry);
    }

    @Override
    public void createTravel(Travel travel, Carry<Travel> carry) {
        repository.createTravel(travel, carry);
    }

    @Override
    public void setTravel(String id, Travel travel, Carry<Success> carry) {
        repository.setTravel(id, travel, carry);
    }

    @Override
    public void deleteTravel(String id, Carry<Success> carry) {
        repository.deleteTravel(id, carry);
    }
}
