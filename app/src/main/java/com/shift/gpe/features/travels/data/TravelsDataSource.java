package com.shift.gpe.features.travels.data;

import com.shift.gpe.features.travels.domain.model.Success;
import com.shift.gpe.features.travels.domain.model.Travel;
import com.shift.gpe.network.Carry;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface TravelsDataSource {

    void getTravels(Carry<List<Travel>> carry);

    void getTravel(String id, Carry<Travel> carry);

    void setTravel(String id, Travel travel, Carry<Success> carry);

    void createTravel(Travel travel, Carry<Travel> carry);

    void deleteTravel(String id, Carry<Success> carry);
}
