package com.shift.gpe.features.travels.presentation;

import com.shift.gpe.features.MvpPresenter;
import com.shift.gpe.features.travels.domain.TravelsInteractor;
import com.shift.gpe.features.travels.domain.model.Success;
import com.shift.gpe.features.travels.domain.model.Travel;
import com.shift.gpe.network.Carry;

import java.util.List;

final class TravelListPresenter extends MvpPresenter<TravelListView> {

    private final TravelsInteractor interactor;

    TravelListPresenter(TravelsInteractor interactor) {
        this.interactor = interactor;
    }

    @Override
    protected void onViewReady() {
        loadTravels();
    }

    private void loadTravels() {
        view.showProgress();
        interactor.loadTravels(new Carry<List<Travel>>() {

            @Override
            public void onSuccess(List<Travel> result) {
                view.showTravelList(result);
                view.hideProgress();
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideProgress();
                view.showError(throwable.getMessage());
            }

        });
    }

    void onTravelSelected(Travel travel) {
        view.showProgress();
        interactor.loadTravel(travel.getIdTravel(), new Carry<Travel>() {

            @Override
            public void onSuccess(Travel result) {
                view.hideProgress();
                // do something
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideProgress();
                view.showError(throwable.getMessage());
            }

        });
    }

    void onTravelLongClicked(Travel travel) {
        view.showProgress();
        interactor.deleteTravel(travel.getIdTravel(), new Carry<Success>() {

            @Override
            public void onSuccess(Success result) {
                loadTravels();
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.hideProgress();
                view.showError(throwable.getMessage());
            }
        });
    }

    public void onCreateTravelClicked(String name) {

        Travel travel = new Travel("-1", "user", name);
        interactor.createTravel(travel, new Carry<Travel>() {
            @Override
            public void onSuccess(Travel result) {
                loadTravels();
            }

            @Override
            public void onFailure(Throwable throwable) {
                view.showError(throwable.getMessage());
            }
        });
    }

}
