package com.shift.gpe.features.travels.presentation;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.shift.gpe.R;
import com.shift.gpe.features.BaseActivity;
import com.shift.gpe.features.MvpPresenter;
import com.shift.gpe.features.MvpView;
import com.shift.gpe.features.travels.domain.model.Travel;

import java.util.List;

public class TravelActivity extends BaseActivity implements TravelListView {


    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private Button createBookButton;
    private TravelAdapter adapter;

    TravelListPresenter presenter;

    @Override
    protected MvpPresenter<TravelListView> getPresenter() {
        presenter = PresenterFactory.createPresenter(this);
        return presenter;
    }

    @Override
    protected MvpView getMvpView() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_travels);
        initView();
    }

    private void initView() {
        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.recyclerView);
        //createBookButton = findViewById(R.id.create_button);

//        createBookButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                presenter.onCreateBookClicked();
//            }
//        });

        adapter = new TravelAdapter(this, new TravelAdapter.SelectTravelListener() {
            @Override
            public void onTravelSelect(Travel travel) {
                presenter.onTravelSelected(travel);
            }

            @Override
            public void onTravelLongClick(Travel travel) {
                presenter.onTravelLongClicked(travel);
            }
        });

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }


    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTravelList(List<Travel> list) {
        adapter.setTravels(list);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
