package com.shift.gpe.features.travels.data;

import com.shift.gpe.features.travels.domain.model.Success;
import com.shift.gpe.features.travels.domain.model.Travel;
import com.shift.gpe.network.Carry;

import java.util.List;

public final class TravelsRepositoryImpl implements TravelsRepository{

    private final TravelsDataSource dataSource;

    public TravelsRepositoryImpl(TravelsDataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void loadTravels(Carry<List<Travel>> carry) {
        dataSource.getTravels(carry);
    }

    @Override
    public void loadTravel(String id, Carry<Travel> carry) {
        dataSource.getTravel(id, carry);
    }

    @Override
    public void createTravel(Travel travel, Carry<Travel> carry) {
        dataSource.createTravel(travel, carry);
    }

    @Override
    public void setTravel(String id, Travel travel, Carry<Success> carry) {
        dataSource.setTravel(id, travel, carry);
    }

    @Override
    public void deleteTravel(String id, Carry<Success> carry) {
        dataSource.deleteTravel(id, carry);
    }
}
