package com.shift.gpe.features.travels.presentation;

import android.content.Context;

import com.shift.gpe.App;
import com.shift.gpe.features.travels.data.TravelApi;
import com.shift.gpe.features.travels.data.TravelsDataSource;
import com.shift.gpe.features.travels.data.TravelsDataSourceImpl;
import com.shift.gpe.features.travels.data.TravelsRepository;
import com.shift.gpe.features.travels.data.TravelsRepositoryImpl;
import com.shift.gpe.features.travels.domain.TravelsInteractor;
import com.shift.gpe.features.travels.domain.TravelsInteractorImpl;

final class PresenterFactory {

    static TravelListPresenter createPresenter(Context context) {
        final TravelApi api = App.getRetrofitProvider(context)
                .getRetrofit()
                .create(TravelApi.class);

        final TravelsDataSource dataSource = new TravelsDataSourceImpl(api);
        final TravelsRepository repository = new TravelsRepositoryImpl(dataSource);
        final TravelsInteractor interactor = new TravelsInteractorImpl(repository);

        return new TravelListPresenter(interactor);
    }
}
