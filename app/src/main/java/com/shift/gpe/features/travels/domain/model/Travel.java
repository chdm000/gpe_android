package com.shift.gpe.features.travels.domain.model;

public class Travel {
    private String idTravel;
    private String idUser;
    private String name;

    public Travel(String idTravel, String idUser, String name) {
        this.idTravel = idTravel;
        this.idUser = idUser;
        this.name = name;
    }

    public String getIdTravel() {
        return idTravel;
    }

    public void setIdTravel(String idTravel) {
        this.idTravel = idTravel;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
