package com.shift.gpe.features.travels.data;

import com.shift.gpe.features.travels.domain.model.Success;
import com.shift.gpe.features.travels.domain.model.Travel;
import com.shift.gpe.network.Carry;
import com.shift.gpe.network.DefaultCallback;

import java.util.List;

public class TravelsDataSourceImpl implements TravelsDataSource {

    private final TravelApi travelApi;

    public TravelsDataSourceImpl(TravelApi travelApi) {
        this.travelApi = travelApi;
    }

    @Override
    public void getTravels(Carry<List<Travel>> carry) {
        travelApi.getTravelList().enqueue(new DefaultCallback(carry));
    }

    @Override
    public void getTravel(String id, Carry<Travel> carry) {
        travelApi.getTravel(id).enqueue(new DefaultCallback(carry));
    }

    @Override
    public void createTravel(Travel travel, Carry<Travel> carry) {
        travelApi.createTravel(travel).enqueue(new DefaultCallback(carry));
    }

    @Override
    public void setTravel(String id, Travel travel, Carry<Success> carry) {
        travelApi.setTravel(id, travel).enqueue(new DefaultCallback(carry));
    }

    @Override
    public void deleteTravel(String id, Carry<Success> carry) {
        travelApi.deleteTravel(id).enqueue(new DefaultCallback(carry));
    }
}
