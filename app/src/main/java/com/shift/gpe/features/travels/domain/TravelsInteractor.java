package com.shift.gpe.features.travels.domain;

import com.shift.gpe.features.travels.domain.model.Success;
import com.shift.gpe.features.travels.domain.model.Travel;
import com.shift.gpe.network.Carry;

import java.util.List;
import java.util.TreeMap;

public interface TravelsInteractor {

    void loadTravels(Carry<List<Travel>> carry);

    void loadTravel(String id, Carry<Travel> carry);

    void createTravel(Travel travel, Carry<Travel> carry);

    void setTravel(String id, Travel travel, Carry<Success> carry);

    void deleteTravel(String id, Carry<Success> carry);
}
