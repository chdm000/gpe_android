package com.shift.gpe.features.login.domain.model;

public class User {
    private String sessionId;
    private String password;
    private String name;

    public User(String sessionId, String name, String password) {
        this.sessionId = sessionId;
        this.name = name;
        this.password = password;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getName() {
        return name;
    }
}
