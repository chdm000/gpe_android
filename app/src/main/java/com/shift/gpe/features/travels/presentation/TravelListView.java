package com.shift.gpe.features.travels.presentation;

import com.shift.gpe.features.MvpView;
import com.shift.gpe.features.travels.domain.model.Travel;

import java.util.List;

public interface TravelListView extends MvpView {

    void showProgress();

    void hideProgress();

    void showTravelList(List<Travel> list);

    void showError(String message);
}
