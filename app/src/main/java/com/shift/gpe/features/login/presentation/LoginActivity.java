package com.shift.gpe.features.login.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.shift.gpe.R;
import com.shift.gpe.features.travels.presentation.TravelActivity;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init_state();
    }

    /**
     * Костыльная реализация loginActivity
     */
    private void init_state() {
        Button b = findViewById(R.id.button_signIn);
        EditText login = findViewById(R.id.editText);
        EditText passwd = findViewById(R.id.editText);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openNoteListScreen();
            }
        });
    }

    public void openNoteListScreen() {
        Intent intent = new Intent(LoginActivity.this, TravelActivity.class);
        startActivity(intent);
    }
}
